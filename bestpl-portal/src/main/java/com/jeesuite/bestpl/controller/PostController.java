package com.jeesuite.bestpl.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.eventbus.AsyncEventBus;
import com.jeesuite.bestpl.api.IPostService;
import com.jeesuite.bestpl.dto.Comment;
import com.jeesuite.bestpl.dto.IdNamePair;
import com.jeesuite.bestpl.dto.Message;
import com.jeesuite.bestpl.dto.Page;
import com.jeesuite.bestpl.dto.PageMetaInfo;
import com.jeesuite.bestpl.dto.PageQueryParam;
import com.jeesuite.bestpl.dto.Post;
import com.jeesuite.common.util.BeanCopyUtils;
import com.jeesuite.passport.LoginContext;
import com.jeesuite.passport.model.LoginUserInfo;

@Controller
@RequestMapping("/post")
public class PostController {

	@Autowired
	private IPostService service;
	
	@Autowired
	private AsyncEventBus asyncEventBus;
	
	@RequestMapping(value = "categorys", method = RequestMethod.GET)
	public @ResponseBody List<IdNamePair> findAllPostCategory(){
		return service.findAllPostCategory();
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public String view(Model model,@PathVariable("id") int id){
		model.addAttribute(PageMetaInfo.ATTR_NAME, PageMetaInfo.current().setModelName("post"));
		Post post = service.findPostById(id);
		model.addAttribute("data", post);
		//同类热门
		List<Post> sameCatePosts = service.findTopPost(post.getCategoryId(), "hot", 10);
		model.addAttribute("hotPosts", sameCatePosts);
		//关联帖子
		
		return "post/view";
	}
	
	@RequestMapping(value = "add", method = RequestMethod.GET)
	public String addPage(Model model){
		model.addAttribute(PageMetaInfo.ATTR_NAME, PageMetaInfo.current().setModelName("post"));
		return "post/add";
	}
	
	@RequestMapping(value = "add", method = RequestMethod.POST)
	public @ResponseBody Boolean addAction(@RequestBody Map<String, Object> params){
		LoginUserInfo loginUserInfo = LoginContext.getLoginUserInfo();
		Post post = BeanCopyUtils.mapToBean(params, Post.class);
		post.setUserId(loginUserInfo.getId());
		post.setUserName(loginUserInfo.getUsername());
		service.addPosts(post);
		return true;
	}
	
	@RequestMapping(value = "page/{sortType}/{pageNo}", method = RequestMethod.GET)
	public String page(Model model,@PathVariable(value="sortType") String sortType,@PathVariable(value="pageNo") int pageNo){
		
		model.addAttribute(PageMetaInfo.ATTR_NAME, PageMetaInfo.current().setModelName("post"));
		PageQueryParam param = new PageQueryParam(pageNo);
		param.setOrderBy(sortType);
		Page<Post> Page = service.pageQueryPost(param);
		model.addAttribute("page", Page);
		model.addAttribute("sortType", sortType);
		return "post/page";
	}

	
	@RequestMapping(value = "add_comment", method = RequestMethod.POST)
	public @ResponseBody Integer addPostComment(@RequestBody Comment comment){
		
		Post post = service.findPostById(comment.getPostId());
		
		LoginUserInfo userInfo = LoginContext.getLoginUserInfo();
		comment.setUserId(userInfo.getId());
		comment.setUserName(userInfo.getUsername());
		service.addPostComment(comment);
		//通知帖子作者评论内容
		Message message = new Message();
		message.setFromUid(userInfo.getId());
		message.setFromUname(userInfo.getUsername());
		message.setToUid(post.getUserId());
		message.setToUname(post.getUserName());
		message.setContent(String.format("用户 %s 评论了你的帖子‘%s’", userInfo.getUsername(),post.getTitle()));
		
		asyncEventBus.post(message);
		
		return comment.getId();
	}
	
	@RequestMapping(value = "comments/{postId}/{page}", method = RequestMethod.GET)
	public @ResponseBody Page<Comment> comments(@PathVariable(value="postId") int postId,@PathVariable(value="page") int pageNo){
		Page<Comment> page = service.pageQueryPostComment(postId, pageNo, 15);
		return page;
	}
}
