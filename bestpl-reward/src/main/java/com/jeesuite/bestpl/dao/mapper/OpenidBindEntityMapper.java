package com.jeesuite.bestpl.dao.mapper;

import org.apache.ibatis.annotations.Param;

import com.jeesuite.bestpl.dao.entity.OpenidBindEntity;
import com.jeesuite.mybatis.plugin.cache.annotation.Cache;

import tk.mybatis.mapper.common.BaseMapper;

public interface OpenidBindEntityMapper extends BaseMapper<OpenidBindEntity> {
	
	@Cache
	OpenidBindEntity findByOpenIdAndType(@Param("openId") String openId,@Param("openType") String type);
}