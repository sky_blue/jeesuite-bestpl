package com.jeesuite.bestpl.dao.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.jeesuite.bestpl.dao.entity.PostEntity;
import com.jeesuite.cache.CacheExpires;
import com.jeesuite.mybatis.plugin.cache.annotation.Cache;
import com.jeesuite.mybatis.plugin.pagination.Page;
import com.jeesuite.mybatis.plugin.pagination.PageParams;

import tk.mybatis.mapper.common.BaseMapper;

public interface PostEntityMapper extends BaseMapper<PostEntity> {
	 

	@Cache(expire=CacheExpires.IN_5MINS)
	List<PostEntity> findTopPosts(@Param("categoryId") Integer categoryId,@Param("orderBy") String orderBy,@Param("limit") int limit);
	
	@Cache(expire=CacheExpires.IN_5MINS)
	Page<PostEntity> findByConditons(@Param("pageParam") PageParams page ,@Param("condition") Map<String, Object> conditions,@Param("orderBy") String orderBy);
	
	int updateCount(@Param("type") String type,@Param("postId") int postId,@Param("addCount") int addCount);
}