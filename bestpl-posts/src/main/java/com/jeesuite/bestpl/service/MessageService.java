package com.jeesuite.bestpl.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jeesuite.bestpl.api.IMessageService;
import com.jeesuite.bestpl.dao.entity.MessageEntity;
import com.jeesuite.bestpl.dao.mapper.MessageEntityMapper;
import com.jeesuite.bestpl.dto.Message;
import com.jeesuite.common.util.BeanCopyUtils;

@Service
public class MessageService implements IMessageService{

	@Autowired
	private MessageEntityMapper messageMapper;
	
	@Override
	public List<Message> findUserNotReadMessage(int userId) {
		List<MessageEntity> list = messageMapper.findNotReadMsgByUserId(userId);
		return BeanCopyUtils.copy(list, Message.class);
	}

	@Override
	public void addMessage(Message message) {
		MessageEntity entity = BeanCopyUtils.copy(message, MessageEntity.class);
		entity.setIsRead(false);
		entity.setCreatedAt(new Date());
		
		messageMapper.insertSelective(entity);
	}

	
}
