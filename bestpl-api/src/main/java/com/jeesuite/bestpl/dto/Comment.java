package com.jeesuite.bestpl.dto;

import java.io.Serializable;
import java.util.Date;

import com.jeesuite.common.util.DateUtils;

public class Comment implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer postId;
	private Integer userId;
	private String userName;

	/**
	 * 点赞人数
	 */
	private Integer likeCount;

	/**
	 * 创建时间
	 */
	private Date createdAt;

	/**
	 * 语言名称
	 */
	private String content;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPostId() {
		return postId;
	}

	public void setPostId(Integer postId) {
		this.postId = postId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getLikeCount() {
		return likeCount;
	}

	public void setLikeCount(Integer likeCount) {
		this.likeCount = likeCount;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public String getFormatCreateTime(){
		if(getCreatedAt() == null)return null;
		long diffSeconds = DateUtils.getDiffSeconds(new Date(), getCreatedAt());
		if(diffSeconds >= 2592000){
			return (diffSeconds/2592000) + " 月前";
		}
		if(diffSeconds >= 86400){
			return (diffSeconds/86400) + " 天前";
		}
		if(diffSeconds >= 3600){
			return (diffSeconds/3600) + " 小时前";
		}
		if(diffSeconds >= 60){
			return (diffSeconds/60) + " 分钟前";
		}
		
		return diffSeconds + " 秒前";
		
	}
}
